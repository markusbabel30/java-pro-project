import java.io.*;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface Save {}

public class Main {

    public static void main(String[] args) {
        MyClass myObj = new MyClass(10, "hello");
        String filename = "data.txt";

        try {
            saveObject(myObj, filename);
            MyClass deserializedObj = (MyClass) loadObject(MyClass.class, filename);
            System.out.println(deserializedObj.getField1()); // 10
            System.out.println(deserializedObj.getField2()); // hello
        } catch (IOException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static void saveObject(Object obj, String filename) throws IOException, IllegalAccessException {
        Main main = new Main();
        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filename)))) {
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Save.class)) {
                    field.setAccessible(true);
                    Class<?> type = field.getType();
                    if (type == int.class) {
                        out.writeInt((int) field.get(obj));
                    } else if (type == long.class) {
                        out.writeLong((long) field.get(obj));
                    } else if (type == double.class) {
                        out.writeDouble((double) field.get(obj));
                    } else if (type == float.class) {
                        out.writeFloat((float) field.get(obj));
                    } else if (type == boolean.class) {
                        out.writeBoolean((boolean) field.get(obj));
                    } else if (type == char.class) {
                        out.writeChar((char) field.get(obj));
                    } else if (type == byte.class) {
                        out.writeByte((byte) field.get(obj));
                    } else if (type == short.class) {
                        out.writeShort((short) field.get(obj));
                    } else {
                        out.writeUTF((String) field.get(obj));
                    }
                }
            }
        }
    }

    public static Object loadObject(Class<?> cls, String filename) throws IOException, IllegalAccessException, InstantiationException {
        Object obj = cls.newInstance();
        try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)))) {
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Save.class)) {
                    field.setAccessible(true);
                    Class<?> type = field.getType();
                    if (type == int.class) {
                        field.setInt(obj, in.readInt());
                    } else if (type == long.class) {
                        field.setLong(obj, in.readLong());
                    } else if (type == double.class) {
                        field.setDouble(obj, in.readDouble());
                    } else if (type == float.class) {
                        field.setFloat(obj, in.readFloat());
                    } else if (type == boolean.class) {
                        field.setBoolean(obj, in.readBoolean());
                    } else if (type == char.class) {
                        field.setChar(obj, in.readChar());
                    } else if (type == byte.class) {
                        field.setByte(obj, in.readByte());
                    } else if (type == short.class) {
                        field.setShort(obj, in.readShort());
                    } else {
                        field.set(obj, in.readUTF());
                    }
                }
            }
        }
        return obj;
    }

    public static class MyClass {
        @Save
        private int field1;
        @Save
        private String field2;

        public MyClass() {
        }

        public MyClass(int field1, String field2) {
            this.field1 = field1;
            this.field2 = field2;
        }

        public int getField1() {
            return field1;
        }

        public String getField2() {
            return field2;
        }

        public void setField1(int field1) {
            this.field1 = field1;
        }

        public void setField2(String field2) {
            this.field2 = field2;
        }
    }
}
