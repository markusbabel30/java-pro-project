import java.lang.annotation.*;
import java.lang.reflect.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Test {
    int a();
    int b();
}

class MyClass {
    @Test(a=2, b=5)
    public void test(int a, int b) {
        System.out.println("a: " + a + ", b: " + b);
    }
}

public class Main {
    public static void main(String[] args) throws Exception {
        MyClass obj = new MyClass();
        Method[] methods = obj.getClass().getDeclaredMethods();

        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                Test annotation = method.getAnnotation(Test.class);
                int a = annotation.a();
                int b = annotation.b();
                method.invoke(obj, a, b);
            }
        }
    }
}
