package ua.kiev.prog;

public interface EntityIntf {
    String getName();
    int getAge();
}
