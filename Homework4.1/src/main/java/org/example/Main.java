package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONObject;

public class Main {
    public static void main(String[] args) {
        try {
            JSONArray jsonArray = getCurrencyRates();
            System.out.println("Number of items in the array: " + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int currencyCodeA = jsonObject.getInt("currencyCodeA");
                int currencyCodeB = jsonObject.getInt("currencyCodeB");
                double rateSell = jsonObject.getDouble("rateSell");
                double rateBuy = jsonObject.getDouble("rateBuy");
                if (currencyCodeA == 840 && currencyCodeB == 980) {
                    System.out.println("Курс гривні до долара: " + rateBuy);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("An error occurred while getting currency rates.");
        }
    }

    private static JSONArray getCurrencyRates() throws IOException {
        URL url = new URL("https://api.monobank.ua/bank/currency");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        int status = con.getResponseCode();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            return new JSONArray(content.toString());
        } finally {
            con.disconnect();
        }
    }
}
