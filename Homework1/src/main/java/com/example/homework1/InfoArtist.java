package com.example.homework1;

public class InfoArtist {
    private String name;
    private String realName;
    private int age;
    private String birthplace;
    private String email;
    private String gender;
    private String genre;

    public InfoArtist(String name, String realName, int age, String birthplace, String email, String gender, String genre) {
        this.name = name;
        this.realName = realName;
        this.age = age;
        this.birthplace = birthplace;
        this.email = email;
        this.gender = gender;
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public String getRealName() {
        return realName;
    }

    public int getAge() {
        return age;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public String getGenre() {
        return genre;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "InfoArtist{" +
                "name='" + name + '\'' +
                ", realName='" + realName + '\'' +
                ", age=" + age +
                ", birthplace='" + birthplace + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}

