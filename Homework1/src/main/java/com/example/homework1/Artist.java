package com.example.homework1;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;

public class Artist extends HttpServlet {

    private List<InfoArtist> artists = Arrays.asList(
            new InfoArtist("Jay-Z", "Shawn Corey Carter", 51, "Brooklyn, New York, U.S.",
                    "jayz@gmail.com", "man", "rap"), // 1
            new InfoArtist("Eminem", "Marshall Bruce Mathers III", 49, "Detroit, Michigan, U.S.",
                    "eminem@gmail.com", "man", "rap"), // 2
            new InfoArtist("Beyonce", "Beyoncé Giselle Knowles-Carter", 40, "Houston, Texas, U.S.",
                    "beyonce@gmail.com", "woman", "R&B"), // 3
            new InfoArtist("Kanye West", "Kanye Omari West", 44, "Atlanta, Georgia, U.S.",
                    "kanyewest@gmail.com", "man", "hip-hop"), // 4
            new InfoArtist("Drake", "Aubrey Drake Graham", 35, "Toronto, Ontario, Canada",
                    "drake@gmail.com", "man", "hip-hop"), // 5
            new InfoArtist("Alicia Keys", "Alicia Augello Cook", 41, "New York City, New York, U.S.",
                    "aliciakeys@gmail.com", "woman", "R&B"), // 6
            new InfoArtist("Rihanna", "Robyn Rihanna Fenty", 33, "Saint Michael, Barbados",
                    "rihanna@gmail.com", "woman", "pop"), // 7
            new InfoArtist("J. Cole", "Jermaine Lamar Cole", 36, "Frankfurt, Germany",
                    "jcole@gmail.com", "man", "hip-hop"), // 8
            new InfoArtist("SZA", "Solána Imani Rowe", 31, "St. Louis, Missouri, U.S.",
                    "sza@gmail.com", "woman", "R&B"), // 9
            new InfoArtist("The Weeknd", "Abel Makkonen Tesfaye", 31, "Toronto, Ontario, Canada",
                    "theweeknd@gmail.com", "man", "R&B")); // 10


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        request.setAttribute("artistList", artists);
        request.getRequestDispatcher("/artists.jsp").forward(request,response);
    }
}

