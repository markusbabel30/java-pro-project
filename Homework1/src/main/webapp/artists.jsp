<%@ page import="java.util.List" %>
<%@ page import="com.example.homework1.InfoArtist" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Artists</title>
    <style>
        body {
            background-color: #F8F8FF;
            font-family: Arial, sans-serif;
        }

        h1 {
            font-size: 36px;
            text-align: center;
            margin-top: 30px;
        }

        table {
            width: 90%;
            margin: 50px auto;
            border-collapse: separate;
            border-spacing: 0;
            border-radius: 10px;
            border: 2px solid #555555;
            box-shadow: 5px 5px 5px #888888;
        }

        th {
            background-color: #D3D3D3;
            color: #222222;
            font-size: 18px;
            font-weight: bold;
            padding: 10px 20px;
            text-align: center;
            border: 2px solid #555555;
        }

        td {
            background-color: #F5F5F5;
            color: #222222;
            font-size: 16px;
            font-weight: normal;
            padding: 10px 20px;
            text-align: center;
            border: 2px solid #555555;
            transition: background-color 0.3s ease;
        }

        tr:hover td {
            background-color: #B0C4DE;
            color: #ffffff;
            cursor: pointer;
        }

        .selected {
            background-color: #FFA07A !important;
            color: #ffffff !important;
        }

        @keyframes tableBounce {
            0% {
                transform: translate(0, 0);
            }
            25% {
                transform: translate(-5px, -10px);
            }
            50% {
                transform: translate(0, 0);
            }
            75% {
                transform: translate(5px, -10px);
            }
            100% {
                transform: translate(0, 0);
            }
        }

        .table-bounce {
            animation: tableBounce 1s ease-in-out;
        }
    </style>
</head>
<body>
<h1>List of Artists</h1>
<table class="table-bounce">
    <thead>
    <tr>
        <th>Name</th>
        <th>Real Name</th>
        <th>Age</th>
        <th>Birthplace</th>
        <th>Email</th>
        <th>Gender</th>
        <th>Genre</th>
    </tr>
    </thead>
    <tbody>
    <% List<InfoArtist> artists = (List<InfoArtist>) request.getAttribute("artistList");
        for(InfoArtist artist : artists){ %>
    <tr>
        <td><%= artist.getName() %></td>
        <td><%= artist.getRealName() %></td>
        <td><%= artist.getAge() %></td>
        <td><%= artist.getBirthplace() %></td>
        <td><%= artist.getEmail() %></td>
        <td><%= artist.getGender() %></td>
        <td><%= artist.getGenre() %></td>
    </tr>
    <% } %>
    </tbody>
</table>
</body>
</html>
