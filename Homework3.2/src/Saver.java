import java.io.*;
import java.lang.annotation.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface SaveTo {
    String path();
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface SaveMethod {
}

@SaveTo(path = "C:\\Users\\marku\\Desktop\\Java Pro Project\\Homework3.2\\file.txt")
class TextContainer {
    String text = "some text";

    @SaveMethod
    public void save(String path) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Saver {
    public static void main(String[] args) {
        TextContainer container = new TextContainer();
        Class<?> cls = container.getClass();

        if (cls.isAnnotationPresent(SaveTo.class)) {
            SaveTo saveTo = cls.getAnnotation(SaveTo.class);
            String path = saveTo.path();

            for (Method method : cls.getDeclaredMethods()) {
                if (method.isAnnotationPresent(SaveMethod.class)) {
                    try {
                        method.invoke(container, path);
                        System.out.printf("Text has been saved to %s%n", path);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            System.out.println("TextContainer class is not annotated with @SaveTo");
        }
    }
}
