package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final String url = "jdbc:mysql://localhost:3306/apartments_db";
    private static final String user = "root";
    private static final String password = "solomia23";
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(url, user, password);
            Scanner scanner = new Scanner(System.in);
            boolean exit = false;
            while (!exit) {
                System.out.println("Select an operation:");
                System.out.println("1. Add a new apartment");
                System.out.println("2. Output all apartments");
                System.out.println("3. Select apartments by district");
                System.out.println("4. Select apartments by area");
                System.out.println("5. Selection of apartments by price");
                System.out.println("6. Exit the program");

                int choice = scanner.nextInt();
                switch (choice) {
                    case 1:
                        addApartment(connection, scanner);
                        break;
                    case 2:
                        getAllApartments(connection);
                        break;
                    case 3:
                        getApartmentsByDistrict(connection, scanner);
                        break;
                    case 4:
                        getApartmentsByArea(connection, scanner);
                        break;
                    case 5:
                        getApartmentsByPrice(connection, scanner);
                        break;
                    case 6:
                        exit = true;
                        break;
                    default:
                        System.out.println("Invalid choice");
                }
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println("Error connecting to the database");
            e.printStackTrace();
        }
    }

    private static void addApartment(Connection connection, Scanner scanner) throws SQLException {
        System.out.println("Enter district:");
        String district = scanner.next();
        System.out.println("Enter address:");
        String address = scanner.next();
        System.out.println("Enter area:");
        float area = scanner.nextFloat();
        System.out.println("Enter number of rooms:");
        int rooms = scanner.nextInt();
        System.out.println("Enter price:");
        float price = scanner.nextFloat();
        String query = "INSERT INTO apartments (district, address, area, rooms, price) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, district);
        preparedStatement.setString(2, address);
        preparedStatement.setFloat(3, area);
        preparedStatement.setInt(4, rooms);
        preparedStatement.setFloat(5, price);
        preparedStatement.executeUpdate();
        System.out.println("Apartment added");
    }

    private static void getAllApartments(Connection connection) throws SQLException {
        String query = "SELECT * FROM apartments";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String district = resultSet.getString("district");
            String address = resultSet.getString("address");
            float area = resultSet.getFloat("area");
            int rooms = resultSet.getInt("rooms");
            float price = resultSet.getFloat("price");
            System.out.println(id + " | " + district + " | " + address + " | " + area + " | " + rooms + " | " + price);
        }
    }

    private static void getApartmentsByDistrict(Connection connection, Scanner scanner) throws SQLException {
        // Get the list of available districts from the database
        String query = "SELECT DISTINCT district FROM apartments";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        List<String> districts = new ArrayList<>();
        while (resultSet.next()) {
            String district = resultSet.getString("district");
            districts.add(district);
        }

        // Display the list of districts in the console with corresponding numbers
        System.out.println("Select a district:");
        for (int i = 0; i < districts.size(); i++) {
            System.out.println((i + 1) + ". " + districts.get(i));
        }

        // Get the district number selected by the user and execute the query
        int districtNumber = scanner.nextInt();
        String district = districts.get(districtNumber - 1);
        query = "SELECT * FROM apartments WHERE district = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, district);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String address = resultSet.getString("address");
            float area = resultSet.getFloat("area");
            int rooms = resultSet.getInt("rooms");
            float price = resultSet.getFloat("price");
            System.out.println(id + " | " + district + " | " + address + " | " + area + " | " + rooms + " | " + price);
        }
    }



    private static void getApartmentsByArea(Connection connection, Scanner scanner) throws SQLException {
        System.out.println("Enter minimum area:");
        float minArea = scanner.nextFloat();
        System.out.println("Enter maximum area:");
        float maxArea = scanner.nextFloat();
        String query = "SELECT * FROM apartments WHERE area BETWEEN ? AND ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setFloat(1, minArea);
        preparedStatement.setFloat(2, maxArea);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String district = resultSet.getString("district");
            String address = resultSet.getString("address");
            float area = resultSet.getFloat("area");
            int rooms = resultSet.getInt("rooms");
            float price = resultSet.getFloat("price");
            System.out.println(id + " | " + district + " | " + address + " | " + area + " | " + rooms + " | " + price);
        }
    }

    private static void getApartmentsByPrice(Connection connection, Scanner scanner) throws SQLException {
        System.out.println("Enter minimum price:");
        float minPrice = scanner.nextFloat();
        System.out.println("Enter maximum price:");
        float maxPrice = scanner.nextFloat();
        String query = "SELECT * FROM apartments WHERE price BETWEEN ? AND ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setFloat(1, minPrice);
        preparedStatement.setFloat(2, maxPrice);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String district = resultSet.getString("district");
            String address = resultSet.getString("address");
            float area = resultSet.getFloat("area");
            int rooms = resultSet.getInt("rooms");
            float price = resultSet.getFloat("price");
            System.out.println(id + " | " + district + " | " + address + " | " + area + " | " + rooms + " | " + price);
        }
    }

}