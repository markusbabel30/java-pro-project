package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("apartments_db");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Menu> criteriaQuery = criteriaBuilder.createQuery(Menu.class);
        Root<Menu> menuRoot = criteriaQuery.from(Menu.class);

        while (true) {
            System.out.println("Please select one of the following options:");
            System.out.println("1. View the entire menu");
            System.out.println("2. Add new menu items");
            System.out.println("3. Select menu items based on price criteria");
            System.out.println("4. Choose dishes only with a discount");
            System.out.println("5. Choose a set of dishes, the total weight of which does not exceed 1 kg, and display them");
            System.out.println("6. Exit the program");

            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    criteriaQuery.select(menuRoot);
                    TypedQuery<Menu> allMenuItems = entityManager.createQuery(criteriaQuery);
                    List<Menu> menuList = allMenuItems.getResultList();
                    System.out.println("All menu items:");
                    for (Menu menu : menuList) {
                        System.out.printf("%s - $%.2f, %.2f kg, %s\n",
                                menu.getDishName(), menu.getPrice(), menu.getWeight(), menu.isDiscountAvailable() ? "Discount available" : "No discount");
                    }
                    break;
                case 2:
                    System.out.println("Enter the name of the dish:");
                    String dishName = scanner.next();
                    System.out.println("Enter the price:");
                    double price = scanner.nextDouble();
                    System.out.println("Enter the weight:");
                    double weight = scanner.nextDouble();
                    System.out.println("Enter discount availability (true/false):");
                    boolean discountAvailable = scanner.nextBoolean();
                    Menu newMenuItem = new Menu(dishName, price, weight, discountAvailable);
                    entityManager.getTransaction().begin();
                    entityManager.persist(newMenuItem);
                    entityManager.getTransaction().commit();
                    System.out.printf("%s - $%.2f, %.2f kg, %s added successfully\n",
                            newMenuItem.getDishName(), newMenuItem.getPrice(), newMenuItem.getWeight(), newMenuItem.isDiscountAvailable() ? "Discount available" : "No discount");
                    break;
                case 3:
                    System.out.println("Enter a minimum price:");
                    double minPrice = scanner.nextDouble();
                    System.out.println("Enter the maximum price:");
                    double maxPrice = scanner.nextDouble();
                    criteriaQuery.select(menuRoot).where(criteriaBuilder.between(menuRoot.get("price"), minPrice, maxPrice));
                    TypedQuery<Menu> menuItemsByPrice = entityManager.createQuery(criteriaQuery);
                    List<Menu> menuItemsByPriceList = menuItemsByPrice.getResultList();
                    System.out.printf("Menu items between $%.2f and $%.2f:\n", minPrice, maxPrice);
                    for (Menu menu : menuItemsByPriceList) {
                        System.out.printf("%s - $%.2f, %.2f kg, %s\n",
                                menu.getDishName(), menu.getPrice(), menu.getWeight(), menu.isDiscountAvailable() ? "Discount available" : "No discount");
                    }
                    break;
                case 4:
                    criteriaQuery.select(menuRoot).where(criteriaBuilder.isTrue(menuRoot.get("discountAvailable")));
                    TypedQuery<Menu> menuItemsWithDiscount = entityManager.createQuery(criteriaQuery);
                    List<Menu> menuItemsWithDiscountList = menuItemsWithDiscount.getResultList();
                    System.out.println("Menu items with discount:");
                    for (Menu menu : menuItemsWithDiscountList) {
                        System.out.printf("%s - $%.2f, %.2f kg, Discount available\n",
                                menu.getDishName(), menu.getPrice(), menu.getWeight());
                    }
                    break;
                case 5:
                    double totalWeight = 0;
                    double totalPrice = 0;
                    List<Menu> selectedMenuItems = new ArrayList<>();
                    while (totalWeight < 1000) {
                        criteriaQuery.select(menuRoot);
                        TypedQuery<Menu> allMenuItemsForWeight = entityManager.createQuery(criteriaQuery);
                        List<Menu> menuListForWeight = allMenuItemsForWeight.getResultList();
                        System.out.println("Choose a dish by number:");
                        for (int i = 0; i < menuListForWeight.size(); i++) {
                            Menu menu = menuListForWeight.get(i);
                            System.out.printf("%d. %s - %.2f kg\n", i + 1, menu.getDishName(), menu.getWeight());
                        }
                        int menuItemNumber = scanner.nextInt();
                        if (menuItemNumber <= 0 || menuItemNumber > menuListForWeight.size()) {
                            System.out.println("Invalid dish number.");
                            continue;
                        }
                        Menu selectedMenuItem = menuListForWeight.get(menuItemNumber - 1);
                        if (totalWeight + selectedMenuItem.getWeight() > 1000) {
                            System.out.println("The weight of dishes exceeds 1 kg.");
                            break;
                        }
                        selectedMenuItems.add(selectedMenuItem);
                        totalWeight += selectedMenuItem.getWeight();
                        totalPrice += selectedMenuItem.getPrice();
                        System.out.println();
                    }
                    System.out.println("You have selected the following dishes:");
                    for (Menu menu : selectedMenuItems) {
                        System.out.println(menu.getDishName() + " - " + menu.getWeight() + " kg");
                    }
                    System.out.printf("Total weight: %.2f kg\n", totalWeight);
                    System.out.printf("Total price: %.2f\n", totalPrice);
                    break;
                case 6:
                    return;
                default:
                    System.out.println("Invalid option selected.");
                    break;
            }
        }
    }
}
