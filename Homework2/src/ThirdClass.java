class ThirdClass extends SecondClass {
    public void publicMethod(@ParameterAnnotation String arg) {
        System.out.println("Public method in ThirdClass with arg: " + arg);
    }

    @MethodAnnotation
    private void privateMethod(String arg1, @ParameterAnnotation String arg2) {
        System.out.println("Private method in ThirdClass with args: " + arg1 + ", " + arg2);
    }

    protected void protectedMethod(int arg) {
        System.out.println("Protected method in ThirdClass with arg: " + arg);
    }

    @MethodAnnotation
    void packagePrivateMethod() {
        System.out.println("Package-private method in ThirdClass");
    }
}