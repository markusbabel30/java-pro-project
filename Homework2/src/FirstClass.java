class FirstClass {
    public void publicMethod() {
        System.out.println("Public method in FirstClass");
    }

    private void privateMethod() {
        System.out.println("Private method in FirstClass");
    }

    protected void protectedMethod(@ParameterAnnotation String arg) {
        System.out.println("Protected method in FirstClass with arg: " + arg);
    }

    void packagePrivateMethod(int arg1, @ParameterAnnotation int arg2) {
        System.out.println("Package-private method in FirstClass with args: " + arg1 + ", " + arg2);
    }
}