class SecondClass extends FirstClass {
    @MethodAnnotation
    public void publicMethod() {
        System.out.println("Public method in SecondClass");
    }

    private void privateMethod(@ParameterAnnotation int arg) {
        System.out.println("Private method in SecondClass with arg: " + arg);
    }

    protected void protectedMethod() {
        System.out.println("Protected method in SecondClass");
    }

    @MethodAnnotation
    void packagePrivateMethod() {
        System.out.println("Package-private method in SecondClass");
    }
}