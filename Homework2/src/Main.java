import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface MethodAnnotation {}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
@interface ParameterAnnotation {}

public class Main {
    public static void main(String[] args) {
        Class<ThirdClass> thirdClass = ThirdClass.class;

        int annotatedMethods = 0;
        int annotatedParameters = 0;

        for (Class<?> clazz = thirdClass; clazz != null; clazz = clazz.getSuperclass()) {
            Method[] methods = clazz.getDeclaredMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(MethodAnnotation.class)) {
                    annotatedMethods++;
                    System.out.println("Method annotation found in " + method.getDeclaringClass().getSimpleName() +
                            "." + method.getName());
                }

                Annotation[][] parameterAnnotations = method.getParameterAnnotations();
                for (int i = 0; i < parameterAnnotations.length; i++) {
                    Annotation[] annotations = parameterAnnotations[i];
                    if (Arrays.stream(annotations).anyMatch(a -> a instanceof ParameterAnnotation)) {
                        annotatedParameters++;
                        System.out.println("Parameter annotation found in " + method.getDeclaringClass().getSimpleName() +
                                "." + method.getName() + ", parameter #" + i);
                    }
                }
            }
        }

        System.out.println("Total method annotations: " + annotatedMethods);
        System.out.println("Total parameter annotations: " + annotatedParameters);
    }
}