package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductsDAO {
    private final Connection connection;

    public ProductsDAO(Connection connection) {
        this.connection = connection;
    }

    public void addProduct(String name, double price) throws SQLException {
        String query = "INSERT INTO products (name, price) VALUES (?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            statement.setDouble(2, price);
            statement.executeUpdate();
        }
    }

    public List<String> getAllProducts() throws SQLException {
        List<String> products = new ArrayList<>();
        String query = "SELECT * FROM products";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                double price = resultSet.getDouble("price");
                products.add("Product name: " + name + ", Product price: " + price);
            }
        }
        return products;
    }
}

class CustomersDAO {
    private final Connection connection;

    public CustomersDAO(Connection connection) {
        this.connection = connection;
    }

    public void addCustomer(String name, String email) throws SQLException {
        String query = "INSERT INTO customers (name, email) VALUES (?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, name);
            statement.setString(2, email);
            statement.executeUpdate();
        }
    }

    public List<String> getAllCustomers() throws SQLException {
        List<String> customers = new ArrayList<>();
        String query = "SELECT * FROM customers";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                customers.add("Customer name: " + name + ", Customer email: " + email);
            }
        }
        return customers;
    }
}

class OrdersDAO {
    private final Connection connection;

    public OrdersDAO(Connection connection) {
        this.connection = connection;
    }

    public void addOrder(int customerId, int productId, int quantity) throws SQLException {
        String query = "INSERT INTO orders (customer_id, product_id, quantity) VALUES (?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, customerId);
            statement.setInt(2, productId);
            statement.setInt(3, quantity);
            statement.executeUpdate();
        }
    }

    public List<String> getAllOrders() throws SQLException {
        List<String> orders = new ArrayList<>();
        String query = "SELECT * FROM orders";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int customerId = resultSet.getInt("customer_id");
                int productId = resultSet.getInt("product_id");
                int quantity = resultSet.getInt("quantity");
                orders.add("Customer ID: " + customerId + ", Product ID: " + productId + ", Quantity: " + quantity);
            }
        }
        return orders;
    }
}