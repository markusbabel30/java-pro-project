package org.example;

import java.sql.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/apartments_db";
        String user = "root";
        String password = "solomia23";

        try (Connection connection = new DatabaseConnection(url, user, password).getConnection()) {
            ProductsDAO productsDAO = new ProductsDAO(connection);
            CustomersDAO customersDAO = new CustomersDAO(connection);
            OrdersDAO ordersDAO = new OrdersDAO(connection);
            Scanner scanner = new Scanner(System.in);
            int choice;
            boolean quit = false;

            do {
                System.out.println("Choose an option:");
                System.out.println("1. Add a new product");
                System.out.println("2. Add a new customer");
                System.out.println("3. Add a new order");
                System.out.println("4. View products");
                System.out.println("5. View customers");
                System.out.println("6. View orders");
                System.out.println("7. Quit");

                choice = scanner.nextInt();

                switch (choice) {
                    case 1:
                        System.out.print("Enter product name: ");
                        String productName = scanner.next();
                        System.out.print("Enter product price: ");
                        double productPrice = scanner.nextDouble();
                        productsDAO.addProduct(productName, productPrice);
                        break;
                    case 2:
                        System.out.print("Enter customer name: ");
                        String customerName = scanner.next();
                        System.out.print("Enter customer email: ");
                        String customerEmail = scanner.next();
                        customersDAO.addCustomer(customerName, customerEmail);
                        break;
                    case 3:
                        System.out.print("Enter customer ID: ");
                        int customerId = scanner.nextInt();
                        System.out.print("Enter product ID: ");
                        int productId = scanner.nextInt();
                        System.out.print("Enter quantity: ");
                        int quantity = scanner.nextInt();
                        ordersDAO.addOrder(customerId, productId, quantity);
                        break;
                    case 4:
                        System.out.println(productsDAO.getAllProducts());
                        break;
                    case 5:
                        System.out.println(customersDAO.getAllCustomers());
                        break;
                    case 6:
                        System.out.println(ordersDAO.getAllOrders());
                        break;
                    case 7:
                        quit = true;
                        break;
                    default:
                        System.out.println("Invalid choice");
                }
            } while (!quit);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}