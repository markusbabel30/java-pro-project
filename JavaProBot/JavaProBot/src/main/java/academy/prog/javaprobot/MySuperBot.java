package academy.prog.javaprobot;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class MySuperBot extends TelegramLongPollingBot {

    private final BotCredentials botCredentials;

    public MySuperBot(TelegramBotsApi telegramBotsApi,
                      BotCredentials botCredentials) {
        super(botCredentials.getBotToken());
        this.botCredentials = botCredentials;

        try {
            telegramBotsApi.registerBot(this);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (!update.hasMessage() || !update.getMessage().hasText())
            return;

        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();

        System.out.println(chatId + " -> " + text);
    }

    @Override
    public String getBotUsername() {
        return botCredentials.getBotName();
    }
}
