package org.example;

import java.util.List;
import java.util.Scanner;
import javax.persistence.*;

public class Main {

    private static final String PERSISTENCE_UNIT_NAME = "apartments_db";
    private static EntityManagerFactory factory;

    public static void main(String[] args) {

        // Ініціалізуємо EntityManagerFactory
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        EntityManager em = factory.createEntityManager();

        boolean exitProgram = false;
        while (!exitProgram) {
            // Зчитуємо параметр, за яким будемо шукати квартири
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose a parameter to search for apartments:");
            System.out.println("1 - district");
            System.out.println("2 - area");
            System.out.println("3 - rooms");
            System.out.println("4 - price");
            System.out.println("5 - show all apartments");
            System.out.println("6 - exit program");
            int parameter = scanner.nextInt();

            // Запускаємо відповідний метод в залежності від вибраного параметру
            switch (parameter) {
                case 1:
                    searchByDistrict(em, scanner);
                    break;
                case 2:
                    searchByArea(em, scanner);
                    break;
                case 3:
                    searchByRooms(em, scanner);
                    break;
                case 4:
                    searchByPrice(em, scanner);
                    break;
                case 5:
                    showAllApartments(em);
                    break;
                case 6:
                    exitProgram = true;
                    break;
                default:
                    System.out.println("Invalid parameter.");
                    break;
            }
        }

        // Закриваємо EntityManager і EntityManagerFactory
        em.close();
        factory.close();
    }

    private static void searchByDistrict(EntityManager em, Scanner scanner) {
        Query query = em.createQuery("SELECT DISTINCT a.district FROM Apartment a");
        List<String> districts = query.getResultList();
        System.out.println("Select a district:\n");

        for (int i = 0; i < districts.size(); i++) {
            System.out.printf("%d. %s\n", (i + 1), districts.get(i));
        }

        int choice = scanner.nextInt();
        String selectedDistrict = districts.get(choice - 1);

        query = em.createQuery("SELECT a FROM Apartment a WHERE a.district = :district");
        query.setParameter("district", selectedDistrict);

        List<Apartment> apartments = query.getResultList();
        System.out.println("\nApartments in " + selectedDistrict + ":\n");
        printApartments(apartments);
    }

    private static void searchByArea(EntityManager em, Scanner scanner) {
        Query query = em.createQuery("SELECT DISTINCT a.area FROM Apartment a");
        List<Double> areas = query.getResultList();
        System.out.println("Select an area:\n");

        for (int i = 0; i < areas.size(); i++) {
            System.out.printf("%d. %.2f\n", (i + 1), areas.get(i));
        }

        int choice = scanner.nextInt();
        double selectedArea = areas.get(choice - 1);

        query = em.createQuery("SELECT a FROM Apartment a WHERE a.area = :area");
        query.setParameter("area", selectedArea);

        List<Apartment> apartments = query.getResultList();
        System.out.println("\nApartments with an area of " + selectedArea + ":\n");
        printApartments(apartments);
    }

    private static void searchByRooms(EntityManager em, Scanner scanner) {
        Query query = em.createQuery("SELECT DISTINCT a.rooms FROM Apartment a");
        List<Integer> rooms = query.getResultList();
        System.out.println("Select number of rooms:\n");

        for (int i = 0; i < rooms.size(); i++) {
            System.out.printf("%d. %d\n", (i + 1), rooms.get(i));
        }

        int choice = scanner.nextInt();
        int selectedRooms = rooms.get(choice - 1);

        query = em.createQuery("SELECT a FROM Apartment a WHERE a.rooms = :rooms");
        query.setParameter("rooms", selectedRooms);

        List<Apartment> apartments = query.getResultList();
        System.out.println("\nApartments with " + selectedRooms + " rooms:\n");
        printApartments(apartments);
    }

    private static void searchByPrice(EntityManager em, Scanner scanner) {
        System.out.println("Enter the minimum price:");
        double minPrice = scanner.nextDouble();
        System.out.println("Enter the maximum price:");
        double maxPrice = scanner.nextDouble();
        Query query = em.createQuery("SELECT a FROM Apartment a WHERE a.price >= :minPrice AND a.price <= :maxPrice");
        query.setParameter("minPrice", minPrice);
        query.setParameter("maxPrice", maxPrice);
        List<Apartment> apartments = query.getResultList();
        System.out.printf("\nApartments with prices between %.2f and %.2f:\n", minPrice, maxPrice);
        printApartments(apartments);
    }

    private static void printApartments(List<Apartment> apartments) {
        System.out.println("Found " + apartments.size() + " apartments:");
        for (Apartment apartment : apartments) {
            System.out.println("ID: " + apartment.getId());
            System.out.println("District: " + apartment.getDistrict());
            System.out.println("Address: " + apartment.getAddress());
            System.out.println("Area: " + apartment.getArea());
            System.out.println("Number of rooms: " + apartment.getRooms());
            System.out.println("Price: " + apartment.getPrice());
            System.out.println();
        }
    }

    public static void showAllApartments(EntityManager em) {
        System.out.println("All apartments:");

        List<Apartment> apartments = em.createQuery("SELECT a FROM Apartment a", Apartment.class).getResultList();

        System.out.printf("%-5s %-30s %-10s %-20s %-10s %s\n", "ID", "Address", "Area", "District", "Price", "Rooms");

        for (Apartment apartment : apartments) {
            System.out.printf("%-5d %-30s %-10.2f %-20s %-10.2f %d\n", apartment.getId(), apartment.getAddress(),
                    apartment.getArea(), apartment.getDistrict(), apartment.getPrice(), apartment.getRooms());
        }
    }

}